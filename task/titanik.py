import pandas as pd
import statistics


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    miss=0
    mr=0
    mrs=0
    ageMiss=[]
    ageMr=[]
    ageMrs=[]
    statistics.median
    for ind in df.index:
        name = df['Name'][ind]
        age = df['Age'][ind]
        if ("Miss." in name):
            if (str(age) == "nan"):
                miss += 1
            else:
                ageMiss.append(int(age))
        if ("Mr." in name):
            if (str(age) == "nan"):
                mr += 1
            else:
                ageMr.append(int(age))
        if ("Mrs." in name):
            if (str(age) == "nan"):
                mrs += 1
            else:
                ageMrs.append(int(age))
                
    age_mr_med = statistics.median(ageMr)
    age_mrs_med = statistics.median(ageMrs)
    age_miss_med = statistics.median(ageMiss)
    return [('Mr.', mr, age_mr_med), ('Mrs.', mrs, age_mrs_med),
                ('Miss.', miss, age_miss_med)]

print(get_filled())
